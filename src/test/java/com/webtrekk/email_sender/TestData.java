package com.webtrekk.email_sender;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.webtrekk.model.Email;

@RunWith(MockitoJUnitRunner.class)
public abstract class TestData {
	
	/*Sample JSON
	[
		{
			"attachmentURL": "https://www.adobe.com/support/products/enterprise/knowledgecenter/media/c4611_sample_explain.pdf",
			"bccList": [],
			"ccList": [],
			"body": "Hi \n\n This is a test E-mail \n\nThanks \nNisar",
			"fromMail": "testnisarapp@gmail.com",
			"password": "TestApp@123",
			"toMail": "nisar6062@yahoo.co.in",
			"subject": "my app test-2"
		}
	]*/
	
	public static List<Email> getEmailList() {
		List<Email> list = new ArrayList<>();
		list.add(new Email.EmailBuilder().subject("subj: 12").body("this is mail").toMail("nisar6062@yahoo.co.in")
				.fromMail("testnisarapp@gmail.com").password("****").build());
		list.add(new Email.EmailBuilder().subject("subj: 15").body("this is mail-2").toMail("nisar6062@yahoo.co.in")
				.fromMail("testnisarapp@gmail.com").password("****").build());
		return list;
	}
	
	public Producer<Long, String> createProducer() {
		Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9042");
		props.put(ProducerConfig.CLIENT_ID_CONFIG, "client-1");
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class.getName());
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		return new KafkaProducer<>(props);
	}
}
