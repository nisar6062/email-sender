package com.webtrekk.email_sender;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.webtrekk.biz.EmailBiz;
import com.webtrekk.controller.EmailController;
import com.webtrekk.model.Email;

/**
 * Unit test for simple App.
 */
public class EmailControllerTest extends TestData {
	private static final ObjectMapper mapper = new ObjectMapper();

	private MockMvc mvc;

	@Mock
	private EmailBiz emailBiz;

	@InjectMocks
	private EmailController emailController;

	@BeforeClass
	public static void setUp() {
		MockitoAnnotations.initMocks(EmailController.class);
	}

	@Before
	public void init() {
		mvc = MockMvcBuilders.standaloneSetup(emailController).dispatchOptions(true).build();
	}

	@Test
	public void testSendEmail() throws Exception {
		List<Email> emailList = TestData.getEmailList();

		doReturn(emailList).when(emailBiz).sendEmail(emailList);
		emailController.sendEmail(emailList);
		String jsonInString = mapper.writeValueAsString(emailList);

		MvcResult result = mvc
				.perform(post("/webtrekk/sendEmail").contentType(MediaType.APPLICATION_JSON).content(jsonInString))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

		final String responseBody = result.getResponse().getContentAsString();
		Assert.assertTrue(responseBody.contains("testnisarapp@gmail.com"));
	}

	@Test
	public void testSendEmailException() throws Exception {
		List<Email> emailList = TestData.getEmailList();

		when(emailBiz.sendEmail(emailList)).thenThrow(JsonProcessingException.class);
		emailController.sendEmail(emailList);
		assertThat(emailController.sendEmail(emailList));
	}
}
