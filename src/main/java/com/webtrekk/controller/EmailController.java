package com.webtrekk.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.webtrekk.biz.EmailBiz;
import com.webtrekk.model.Email;

/**
 * @author Nisar Adappadathil Controller class with API definitions
 */
@RestController
@RequestMapping("webtrekk")
public class EmailController {
	private static final Logger LOGGER = Logger.getLogger(EmailController.class);

	@Autowired
	EmailBiz emailBiz;

	@RequestMapping(value = "/test", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public String test() {
		LOGGER.info("test:");
		return "Success the webtrekk app is running !!!";
	}

	@RequestMapping(value = "/sendEmail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Email>> sendEmail(@RequestBody List<Email> emailList) {
		LOGGER.info("sendEmails :"+ emailList);
		try {
			emailBiz.sendEmail(emailList);
			return new ResponseEntity(emailList, HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return new ResponseEntity(emailList, HttpStatus.BAD_REQUEST);
		}
	}
}
