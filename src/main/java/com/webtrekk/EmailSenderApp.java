package com.webtrekk;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.webtrekk.model.LoggingInterceptor;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
/**
 * @author Nisar Adappadathil
 * Spring Application class with swagger enabled
 */
@EnableSwagger2
@SpringBootApplication
public class EmailSenderApp extends WebMvcConfigurerAdapter {
	
	private static final Logger LOGGER = Logger.getLogger(EmailSenderApp.class);
	
	public static void main(String[] args) {
		LOGGER.info("Started Email Sender Application");
		SpringApplication.run(EmailSenderApp.class, args);
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new LoggingInterceptor()).addPathPatterns("/**");
	}

	@Bean
	public Docket docket() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage(getClass().getPackage().getName())).paths(PathSelectors.any())
				.build().apiInfo(generateApiInfo());
	}

	private ApiInfo generateApiInfo() {
		return new ApiInfo("Email-Sender application", "This service is to help the clients to send emails.",
				"Version 1.0", "", "", "Apache 2.0", "http://www.apache.org/licenses/LICENSE-2.0");
	}
}
