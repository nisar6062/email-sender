package com.webtrekk.biz;

import java.util.Base64;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.webtrekk.EmailSenderApp;
import com.webtrekk.model.Email;

/**
 * @author Nisar Adappadathil Biz class with business logic for sending mail
 */
@Service
public class EmailBiz {
	private static final Logger LOGGER = Logger.getLogger(EmailSenderApp.class);

	@Value("${kafka.brokers}")
	private String kafkaBrokers = "localhost:9042";
	@Value("${kafka.clientId}")
	private String clientId = "Client-1";
	@Value("${kafka.topic}")
	private String topic = "email-topic";
	@Value("${kafka.groupid}")
	private String groupId;
	@Value("${kafka.offset}")
	private String offset;

	private ObjectMapper objMap;

	@Autowired
	public EmailBiz(ObjectMapper objMap) {
		this.objMap = objMap;
	}

	public List<Email> sendEmail(List<Email> emailList) throws JsonProcessingException {
		LOGGER.info("Send Emails :" + emailList);
		Producer<Long, String> producer = createProducer();
		for (Email email : emailList) {
			Long id = new Random().nextLong();
			email.setId(id);
			ProducerRecord<Long, String> record;
			try {
				record = new ProducerRecord<>(topic, id,
						Base64.getEncoder().encodeToString(objMap.writeValueAsString(email).getBytes()));
				producer.send(record);
			} catch (JsonProcessingException e) {
				LOGGER.error(e.getMessage(), e);
				throw e;
			}
		}
		return emailList;
	}

	public Producer<Long, String> createProducer() {
		Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaBrokers);
		props.put(ProducerConfig.CLIENT_ID_CONFIG, clientId);
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class.getName());
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		LOGGER.info("Creating Producer :" + props);
		return new KafkaProducer<>(props);
	}
}
